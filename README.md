# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do FPGA Lattice iCE40UP5K-SG48I (https://www.latticesemi.com/Products/FPGAandCPLD/iCE40UltraPlus).

# Projekt PCB

Schemat: [doc/ICE40UP5K_EVB_V1_0_SCH.pdf](doc/ICE40UP5K_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/ICE40UP5K_EVB_V1_0_3D.pdf](doc/ICE40UP5K_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

Rysunek montażowy: [doc/ICE40UP5K_EVB_V1_0_ASSEMBLY_TOP.pdf](doc/ICE40UP5K_EVB_V1_0_ASSEMBLY_TOP.pdf)

# Licencja

MIT
